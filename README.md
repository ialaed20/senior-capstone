# Senior Capstone

This algorithm will allow the user to type (or copy and paste) the text from an article they have, which is the argument of the algorithm here. Then the algorithm will detect what the most relevant frequent word is, using term frequency, while also transferring the text to the five different classification algorithms, which, based on the majority, returns whether the text is fake or not. 
